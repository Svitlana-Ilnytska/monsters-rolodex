import { useEffect, useState } from "react";
import CardList from "./Components/CardList/CardList";
import SearchBox from "./Components/SearchBox/SearchBox";
import "./App.css";

function App() {
  const [monsters, setMonsters] = useState([]);
  const [searchField, setsearchField] = useState("");

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((users) => setMonsters(users));
  }, []);

  const onSearchChange = (event) => {
    const searchField = event.target.value.toLowerCase();
    setsearchField(searchField);
  };

  const filteredMonsters = monsters.filter((monster) => {
    return monster.name.toLowerCase().includes(searchField);
  });

  return (
    <>
      <h1 className='title'>Monsters Rolodex</h1>
      <SearchBox
        onSearchChangeHandler={onSearchChange}
        placeholder="search monster"
      />
      <CardList monsters={filteredMonsters} />
    </>
  );
}

export default App;
