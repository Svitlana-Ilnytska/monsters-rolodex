import React from "react";

const CardItem = ({ name, id, email }) => (
  <>
    <img
      alt={`monster ${name}`}
      src={`https://robohash.org/${id}?set=set2&size=180x180`}
    />
    <h2>{name}</h2>
    <p>{email}</p>
  </>
);

export default CardItem;
