import React from "react";
import css from "./SearchBox.module.css";

const SearchBox = ( {onSearchChangeHandler, placeholder} ) => (
    <input
    className={css.searchBox}
    type="search"
    placeholder={placeholder}
    onChange={onSearchChangeHandler}
  />
  );
  
  
  export default SearchBox;