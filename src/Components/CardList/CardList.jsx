import React from "react";
import CardItem from "../CardItem/CardItem";
import css from "./CardList.module.css";

const CardList = ({ monsters }) => (
  <ul className={css.cardList}>
    {monsters.map((monster) => {
      const { name, id, email } = monster;
      return (
        <li className={css.cardContainer} key={id}>
        <CardItem name={name} id={id} email={email}/>
          </li>
      );
    })}
  </ul>
);

export default CardList;
